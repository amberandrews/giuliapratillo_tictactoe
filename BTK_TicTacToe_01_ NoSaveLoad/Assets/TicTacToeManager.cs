﻿using UnityEngine;
using UnityEngine.UI;

using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Enum to check if a cell is empty or marked by player 1 or 2
/// </summary>
public enum BoardStateEnum {

	BSE_Empty,
	BSE_Cross,
	BSE_Circle,

}

/// <summary>
/// Class to bundle the state and the corrosponding button together
/// </summary>
[System.Serializable]
public class BoardCell {

	public BoardStateEnum m_state;

	public GameObject m_boardButton;

}

/// <summary>
/// The Tic tac toe - reacts to button presses, updates the board and handles save/load functionality
/// </summary>
public class TicTacToeManager : MonoBehaviour {

	[SerializeField] GameObject scoretextObj;  //text that displays score
	int score1=0,score2=0;
	/// <summary>
	/// is it player ones turn?
	/// </summary>
	bool m_turnPlayerOne = true;

	/// <summary>
	/// is the game won?
	/// </summary>
	bool m_gameWon = false;

	/// <summary>
	/// list of al board game cells (needed to be added in the unity inspector)
	/// </summary>
	[SerializeField] 
	List<BoardCell> m_gameBoard;


	/// <summary>
	/// Textfield for the status of the game 
	/// </summary>
	[SerializeField] 
	Text m_gameStatusTextField;

	/// <summary>
	/// Triggered when the GameBoardButton is pressed - index has to be added in the unity inspector
	/// </summary>
	/// <param name="index">Index.</param>
	public void ButtonPressed(int index)
	{

		BoardCell cell = m_gameBoard[index];

		///if the field is empty mark it with an X or an O 
		if (cell.m_state == BoardStateEnum.BSE_Empty) {
		
			if (m_turnPlayerOne) 
			{
				cell.m_boardButton.GetComponentInChildren<Text> ().text = "X";
				cell.m_state = BoardStateEnum.BSE_Cross;
			} 
			else 	
			{
				cell.m_boardButton.GetComponentInChildren<Text>().text = "O";
				cell.m_state = BoardStateEnum.BSE_Circle;
			}

			//check if we won the game
			if (CheckWinConditions ()) {


				if (m_turnPlayerOne) {
					m_gameStatusTextField.text = "YAY! Player ONE won!"; 
					score1++;                                                           //update the actual score
					scoretextObj.GetComponent<Text> ().text = "Score: " + score1 + "/" + score2;  //update the visual text field
					resetBoard ();

				} else {
					m_gameStatusTextField.text = "YAY! Player TWO won!"; 
					score2++;                                                                 //same thing as on top
					scoretextObj.GetComponent<Text> ().text = "Score: " + score1 + "/" + score2;
					resetBoard ();
				}
			} else {

				//switch the active player
				m_turnPlayerOne = !m_turnPlayerOne;            

				UpdatePlayerStatusText ();
			}
		}
	}
		

	/// <summary>
	/// Checks all conditions that lead to a win in tic tac toe
	/// </summary>
	/// <returns><c>true</c>, if window conditions was checked, <c>false</c> otherwise.</returns>
	bool CheckWinConditions()
	{
		BoardStateEnum check;

		///depending which player is active we check for a different state
		if (m_turnPlayerOne) {
			check = BoardStateEnum.BSE_Cross;
		} else {
			check = BoardStateEnum.BSE_Circle;
		}

		///check for the 8 different combinations to win in tic tac toe
		if (m_gameBoard [0].m_state == check && m_gameBoard [1].m_state == check && m_gameBoard [2].m_state == check) {
			return true;
		}
		if (m_gameBoard [3].m_state == check && m_gameBoard [4].m_state == check && m_gameBoard [5].m_state == check) {
			return true;
		}
		if (m_gameBoard [6].m_state == check && m_gameBoard [7].m_state == check && m_gameBoard [8].m_state == check) {
			return true;
		}
		if (m_gameBoard [0].m_state == check && m_gameBoard [3].m_state == check && m_gameBoard [6].m_state == check) {
			return true;
		}
		if (m_gameBoard [1].m_state == check && m_gameBoard [4].m_state == check && m_gameBoard [7].m_state == check) {
			return true;
		}
		if (m_gameBoard [2].m_state == check && m_gameBoard [5].m_state == check && m_gameBoard [8].m_state == check) {
			return true;
		}
		if (m_gameBoard [0].m_state == check && m_gameBoard [4].m_state == check && m_gameBoard [8].m_state == check) {
			return true;
		}
		if (m_gameBoard [2].m_state == check && m_gameBoard [4].m_state == check && m_gameBoard [6].m_state == check) {
			return true;
		}

		return false;
	}

	/// <summary>
	/// writes the current game status text, dependend on which players turn it is
	/// </summary>
	public void UpdatePlayerStatusText()
	{
		if (m_turnPlayerOne) {
			m_gameStatusTextField.text = "Player ONE - Place a X"; 
		} else {
			m_gameStatusTextField.text = "Player TWO - Place a O"; 
		}
	}


	public void saveGame(){        // saves the game with playerprefs

		PlayerPrefs.SetInt ("score1", score1);                                     //saves the scores of the two players as ints
		PlayerPrefs.SetInt ("score2", score2);
		Debug.Log (m_gameBoard [0].m_state.ToString());
		PlayerPrefs.SetString ("tile0", m_gameBoard [0].m_state.ToString());       //saves the tiles sperately, converting them to string
		PlayerPrefs.SetString ("tile1", m_gameBoard [1].m_state.ToString());
		PlayerPrefs.SetString ("tile2", m_gameBoard [2].m_state.ToString());
		PlayerPrefs.SetString ("tile3", m_gameBoard [3].m_state.ToString());
		PlayerPrefs.SetString ("tile4", m_gameBoard [4].m_state.ToString());
		PlayerPrefs.SetString ("tile5", m_gameBoard [5].m_state.ToString());
		PlayerPrefs.SetString ("tile6", m_gameBoard [6].m_state.ToString());
		PlayerPrefs.SetString ("tile7", m_gameBoard [7].m_state.ToString());
		PlayerPrefs.SetString ("tile8", m_gameBoard [8].m_state.ToString());
		PlayerPrefs.SetString ("player1turn", m_turnPlayerOne.ToString());       //same thing but with the player turn (aka having a string that says the boolean value)

	}


	public void loadGame(){         //actually loads the saved things

		score1 = PlayerPrefs.GetInt ("score1");           //sets the int score value to the actual score value
		score2 = PlayerPrefs.GetInt ("score2");
		scoretextObj.GetComponent<Text> ().text = "Score: " + score1 + "/" + score2;  //loads the visual text display of the score

		string tempStringTurn = PlayerPrefs.GetString ("player1turn");  // "provisory" string from the player1 turn

		if (tempStringTurn == "true") {    //changes the turn based on the provisory string
			m_turnPlayerOne = true;
		} else {
			m_turnPlayerOne = false;
		}
		UpdatePlayerStatusText ();

		m_gameBoard [0].m_state = translateString (PlayerPrefs.GetString ("tile0"));
		m_gameBoard [1].m_state = translateString (PlayerPrefs.GetString ("tile1"));        //all the differentstrings from the single tiles,  that become boardStateEnum
		m_gameBoard [2].m_state = translateString (PlayerPrefs.GetString ("tile2"));
		m_gameBoard [3].m_state = translateString (PlayerPrefs.GetString ("tile3"));
		m_gameBoard [4].m_state = translateString (PlayerPrefs.GetString ("tile4"));
		m_gameBoard [5].m_state = translateString (PlayerPrefs.GetString ("tile5"));
		m_gameBoard [6].m_state = translateString (PlayerPrefs.GetString ("tile6"));
		m_gameBoard [7].m_state = translateString (PlayerPrefs.GetString ("tile7"));
		m_gameBoard [8].m_state = translateString (PlayerPrefs.GetString ("tile8"));



		for (int x = 0; x < m_gameBoard.Count; x++) {

			if (m_gameBoard [x].m_state == BoardStateEnum.BSE_Empty) {
				m_gameBoard[x].m_boardButton.GetComponentInChildren<Text>().text = "";  //here (become boardStateEnum) = get their actual content (x,o, -)
			}
			if (m_gameBoard [x].m_state == BoardStateEnum.BSE_Cross) {
				m_gameBoard[x].m_boardButton.GetComponentInChildren<Text>().text = "X";
			}
			if (m_gameBoard [x].m_state == BoardStateEnum.BSE_Circle) {
				m_gameBoard[x].m_boardButton.GetComponentInChildren<Text>().text = "O";
			}
		}



	}

	BoardStateEnum translateString(string input){

		if (input == "BSE_Empty") {
			return BoardStateEnum.BSE_Empty;             //translates the strings back to their "value"
		}
		if (input == "BSE_Cross") {
			return BoardStateEnum.BSE_Cross;
		}
		if(input ==	"BSE_Circle"){
			return BoardStateEnum.BSE_Circle;
		}

		return BoardStateEnum.BSE_Empty; //line needed because eliminating possibility for the if to not be true, because otherwise c# problem
	}

	public void resetGame(){   //resets the whole game to start board and the score

		score1 = 0;        
		score2 = 0;
		scoretextObj.GetComponent<Text> ().text = "Score: " + score1 + "/" + score2;
		resetBoard ();
	}


	void resetBoard(){                // only resets the board and not the score (for when somebody wins)
		m_turnPlayerOne = true;
		UpdatePlayerStatusText ();        //resets who's turn it is 
		for (int x = 0; x < m_gameBoard.Count; x++) {
			m_gameBoard [x].m_state = BoardStateEnum.BSE_Empty;
			m_gameBoard[x].m_boardButton.GetComponentInChildren<Text>().text = "";   //resets tiles visually
		}



	}

}
